from django.conf.urls import include,url
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView

from django.contrib import admin

from recipe import views as recipe_views

from registration.views import RegistrationView
register = RegistrationView.as_view()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^feed/', include('openeats.feed.urls')),
    url(r'^groups/', include('openeats.recipe_groups.urls')),
    url(r'^ingredient/', include('openeats.ingredient.urls')),
    url(r'^list/', include('openeats.list.urls')),
    url(r'^tags/', include('openeats.tags.urls')),
    url(r'^search/', include('haystack.urls')),
    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    
    url(r'^$', recipe_views.index, name="home"),
    url(r'^recipe/', include('openeats.recipe.urls')),
        
    url(r'^oauth/', include('social_django.urls',namespace='social')),
    url(r'^',include('django.contrib.auth.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
