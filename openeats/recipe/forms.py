from django.forms import CharField,ModelForm
from models import Recipe

class RecipeForm(ModelForm):
    description = CharField(required=False)
    class Meta:
        model = Recipe
        fields = ['title','ingredients','instructions','description']
        