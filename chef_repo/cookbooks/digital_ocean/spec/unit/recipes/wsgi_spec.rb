require 'spec_helper'

describe 'digital_ocean::wsgi' do
    context 'When running against a hardened Ubuntu 16.04 platform' do
        let(:chef_run) { ChefSpec::SoloRunner.new(platform: 'ubuntu',version: '16.04').converge(described_recipe) }
        
        it 'should install apache2' do
            expect(chef_run).to install_package('apache2')
        end
        
        it 'should install the mod-wsgi module' do
            expect(chef_run).to install_package('libapache2-mod-wsgi')
        end
        
        it 'should install python dependencies' do
            expect(chef_run).to install_package('python-pip')
            expect(chef_run).to install_package('virtualenv')
        end
        
        it 'should install dependencies necessary for compiled C libraries' do
            expect(chef_run).to install_package('python-dev')
            expect(chef_run).to install_package('build-essential')
        end
        
        it 'should install dependencies specific to openeats' do
            expect(chef_run).to install_package('libxml2-dev')
            expect(chef_run).to install_package('libxslt1-dev')
            expect(chef_run).to install_package('zlib1g-dev')
        end
        
        it 'should upgrade pip to the latest version' do
            expect(chef_run).to run_execute('sudo pip install --upgrade pip')
        end
        
        it 'should create the appropriate apache directory' do
            expect(chef_run).to create_directory('/var/www/home_bistro')
            .with(user: 'devops')
            .with(group: 'devops')
            .with(mode: '0755')
        end
      
        it 'should create a swapfile if none previously exists' do
            expect(chef_run).to run_execute('sudo dd if=/dev/zero of=/swapfile bs=1024 count=524288')
            expect(chef_run).to run_execute('sudo mkswap /swapfile')
            expect(chef_run).to run_execute('sudo swapon /swapfile')
        end
      
        it 'should not create a swapfile if there is one though' do
            allow(::File).to receive(:exists?).and_call_original
            allow(::File).to receive(:exists?).with('/swapfile').and_return(true)
            expect(chef_run).not_to run_execute('sudo dd if=/dev/zero of=/swapfile bs=1024 count=524288')
        end
        
        it 'should install a virtual environment in the apache directory' do
          expect(chef_run).to run_execute('virtualenv /var/www/home_bistro/venv --no-site-packages')
          .with(user: 'devops')
          .with(group: 'devops')
        end
        
        it "should enable the wsgi site and static content via home-bistro.conf" do
            chef_run.node.force_default['disable_ssl'] = true
            chef_run.converge(described_recipe)            
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro.conf').with_content { |content|
                    expect(content).to match(/^\s*WSGIDaemonProcess home_bistro/)
                    expect(content).to include('ServerName example.com')
                    expect(content).to include('ServerAlias www.example.com')
                    expect(content).not_to match(/RewriteEngine on/)
                }
        end
        
        it 'should enable the home-bistro.conf VirtualHost' do
            expect(chef_run).to run_execute('sudo a2ensite home-bistro.conf')
        end
                
        it 'should restart the apache2 service' do
            expect(chef_run).to restart_service('apache2')
        end
    end
end
