from django.conf import settings
from django.contrib.sites.models import Site

def oelogo(context):
    """return the oelogo for templates allowing users to set their own logo in the settings file"""
    return {'OELOGO': settings.STATIC_URL + settings.OELOGO}

def oetitle(context):
    return {'OETITLE': settings.OETITLE}
  
def site(context):
    return {'site': Site.objects.get_current()}
