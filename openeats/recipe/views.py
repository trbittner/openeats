from django.shortcuts import render, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.forms.models import inlineformset_factory
from django.http import HttpResponse, Http404, JsonResponse
from django.contrib.contenttypes.models import ContentType
from django.views.generic import DetailView, ListView
from django.utils.translation import ugettext as _
from django.contrib.auth.mixins import LoginRequiredMixin
from models import Recipe
from openeats.ingredient.models import Ingredient
from forms import RecipeForm
from django.conf import settings
from django.db.models import F
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import *
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFontFamily
import json
from django.views.decorators.http import require_http_methods

@login_required
def index(request):
    current_user = request.user
    recipe_list = Recipe.objects.filter(user_id=current_user.id).order_by('title')
    return render(request,'recipe/index.html', {'recipes': recipe_list})

@login_required
def display(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    
    if recipe and recipe.user_id != request.user.id:
        return HttpResponse(status=403)

    if 'recipe_history' in request.session:
        recipe_history = request.session['recipe_history']
        if [recipe.title, recipe.id] not in recipe_history:
            recipe_history.append(([recipe.title, recipe.id]))
            if len(recipe_history) > 4:
                recipe_history.pop(0)
            request.session['recipe_history'] = recipe_history
    else:
        request.session['recipe_history'] = [[recipe.title, recipe.id]]
        
    return render(request,'recipe/recipe_detail.html',{'recipe': recipe})

@login_required
@require_http_methods(['POST'])
def edit(request):
    pk = request.POST.get('pk')
    if pk:
        not_found_msg = {'status':'error','msg':'The recipe could not be found.'}
        try:
            recipe = Recipe.objects.get(pk=pk)
            if recipe.user_id != request.user.id:
                return JsonResponse(not_found_msg)
        except ObjectDoesNotExist:
            return JsonResponse(not_found_msg)
    else:
        recipe = Recipe()
        recipe.user_id = request.user.id
    
    name = request.POST.get('name')
    value = request.POST.get('value')
    
    if name == None or value == None:
        return JsonResponse({'status':'error','msg':'There was a problem saving the recipe.'})
    setattr(recipe,request.POST.get('name'),request.POST.get('value'))
    recipe.save()
    return HttpResponse(status=200)
