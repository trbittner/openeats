package 'apache2'
package 'libapache2-mod-wsgi'

package 'python-pip'
package 'virtualenv'

package 'python-dev'
package 'build-essential'

# Packages needed for openeats python packages.
package 'libxml2-dev' 
package 'libxslt1-dev' 
package 'zlib1g-dev'

execute 'Upgrade pip' do
    command "sudo pip install --upgrade pip"
end

user_name = node['user_name']
directory '/var/www/home_bistro' do
    owner user_name
    group user_name
    mode '0755'
    action :create
end

unless File.exists?('/swapfile')
    execute 'Allocate swapfile space' do
        command "sudo dd if=/dev/zero of=/swapfile bs=1024 count=524288"
    end
    execute 'Create swapfile' do
        command "sudo mkswap /swapfile"
    end
    execute 'Activate swapfile' do
        command "sudo swapon /swapfile"
    end
end

execute 'Install venv' do
    command "virtualenv /var/www/home_bistro/venv --no-site-packages"
    user user_name
    group user_name
end

droplet_data = data_bag_item('sysadmins','digital_ocean_droplet')
template '/etc/apache2/sites-available/home-bistro.conf' do
    source 'home-bistro.conf.erb'
    action :create
    variables({
        :from => droplet_data['from'],
        :domain => droplet_data['domain'],
        :disable_ssl => node['disable_ssl']
        })
end

execute 'Enable VirtualHost' do
    command 'sudo a2ensite home-bistro.conf'
end

service 'apache2' do
    action :restart
end
