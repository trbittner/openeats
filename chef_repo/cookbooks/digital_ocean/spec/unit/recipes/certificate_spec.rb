require 'spec_helper'

describe 'digital_ocean::certificate' do
    context 'When setting up https' do
        let(:chef_run) { ChefSpec::SoloRunner.new(platform: 'ubuntu',version: '16.04').converge(described_recipe) }
        
        it 'should install certbot' do
            expect(chef_run).to create_remote_file_if_missing('/usr/local/sbin/certbot-auto')
        end

        it 'should enable the ssl module' do
            expect(chef_run).to run_execute('sudo a2enmod ssl')
        end
        
        it 'should enable the key caching' do
            expect(chef_run).to run_execute('sudo a2enmod socache_shmcb')
        end
        
        it 'should enable rewrite rules' do
            expect(chef_run).to run_execute('sudo a2enmod rewrite')
        end
        
        it 'should run certbot to create a certificate if no certificate is present' do
            expect(chef_run).to run_execute(
                "sudo /usr/local/sbin/certbot-auto certonly --apache -n -d example.com -m you --agree-tos --redirect")
        end
        
        it 'should install the ssl conf module by default and update the http conf with rewrite rules' do
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro-le-ssl.conf').with_content { |content| 
                expect(content).to include('<IfModule mod_ssl.c>')
                expect(content).to match(/^\s*WSGIDaemonProcess home_bistro/)
                expect(content).to match(%r[SSLCertificateFile /etc/letsencrypt/live/example.com/fullchain.pem])
            }
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro.conf').with_content { |content|
                    expect(content).not_to match(/^\s*WSGIDaemonProcess home_bistro/)
                    expect(content).to match(/RewriteEngine on/)
                }
        end
        
        it 'should not create a certificate if the file already exists' do
            allow(::File).to receive(:exists?).and_call_original
            allow(::File).to receive(:exists?).with('/etc/letsencrypt/live/example.com/privkey.pem').and_return(true)
            expect(chef_run).to_not run_execute(
                "sudo /usr/local/sbin/certbot-auto certonly --apache -n -d example.com -m you --agree-tos --redirect")
        end

        it 'should update http conf when disable_ssl has been set' do
            chef_run.node.force_default['disable_ssl'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro.conf').with_content { |content|
                    expect(content).to match(/^\s*WSGIDaemonProcess home_bistro/)
                    expect(content).not_to match(/RewriteEngine on/)
                }
            expect(chef_run).to create_template('/etc/apache2/sites-available/home-bistro-le-ssl.conf').with_content { |content| 
                expect(content).not_to match(/^\s*WSGIDaemonProcess home_bistro/)
            }
        end                                

        it 'should create an options-ssl conf file' do
            chef_run.converge(described_recipe)
            expect(chef_run).to create_cookbook_file('/etc/letsencrypt/options-ssl-apache.conf')
        end
        
        it 'should enable the virtual host for the ssl conf' do
            expect(chef_run).to run_execute('sudo a2ensite home-bistro-le-ssl.conf')
        end
        
        it 'should create a cron entry' do
            expect(chef_run).to create_cron('renew certificate')
                .with_minute('30')
                .with_hour('2')
                .with_user('devops')
                .with_mailto('you')
                .with_command("sudo sh -c '/usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log'")
        end
                
        it 'should not disable the ssl module by default' do
            expect(chef_run).not_to run_execute('sudo a2dismod ssl')
        end
        
        it 'should not disable the ssl site by default' do
            expect(chef_run).not_to run_execute('sudo a2dissite home-bistro-le-ssl.conf')
        end
        
        it 'should not enable the http site by default' do
            expect(chef_run).not_to run_execute('sudo a2ensite home-bistro.conf')
        end
        
        it 'should disable the ssl module if directed' do
            chef_run.node.force_default['disable_ssl'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2dismod ssl')
        end
        
        it 'should disable the ssl site if directed' do
            chef_run.node.force_default['disable_ssl'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2dissite home-bistro-le-ssl.conf')
        end
        
        it 'should enable the http site if directed' do
            chef_run.node.force_default['disable_ssl'] = true
            chef_run.converge(described_recipe)
            expect(chef_run).to run_execute('sudo a2ensite home-bistro.conf')
        end
    end
end
