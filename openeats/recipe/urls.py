from django.conf.urls import *

import openeats.recipe.views as recipe_views

urlpatterns = [
    url(r'^new/$', recipe_views.edit, name='recipe_new'),
    url(r'^(?P<id>[0-9]+)/$',recipe_views.display, name='recipe_display'),
    url(r'^edit/$',recipe_views.edit,name='recipe_edit')
]
