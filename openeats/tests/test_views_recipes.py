from django.contrib.auth.models import User
import json
from django.test import Client,TestCase
from ..recipe.models import Recipe

class TestIndexView(TestCase):
    def setUp(self):
        Recipe.objects.create(title="Delicious Recipe",instructions="Foo",user_id=1)
        Recipe.objects.create(title="Fine Dining Recipe",instructions="Bar")
        self.user = User.objects.create(username="testuser")
        self.user.set_password("secret")
        self.user.save()
        self.client = Client()
        
    def test_no_login(self):
        response = self.client.get('/')
        self.assertRedirects(response,'/login/?next=/')
        
    def test_happy_path(self):
        self.client.force_login(self.user)
        response = self.client.get('/')
        self.assertEqual(200,response.status_code)
        self.assertContains(response,'Delicious Recipe')
        self.assertNotContains(response,'Fine Dining Recipe')

class TestDisplayView(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="testuser")
        self.user.set_password("secret")
        self.user.save()         
        self.client = Client()
        self.client.force_login(self.user)
        Recipe.objects.create(title="Recipe 1", instructions="foo", user_id=1)
        Recipe.objects.create(title="Recipe 2", instructions="bar", user_id=1)
        Recipe.objects.create(title="Recipe 3", instructions="baz", user_id=1)
        Recipe.objects.create(title="Recipe 4", instructions="boom", user_id=1)
        Recipe.objects.create(title="Recipe 5", instructions="blam", user_id=1)
        Recipe.objects.create(title="Recipe 6", instructions="kablow")
        
    def test_404_if_no_id_exists(self):
        response = self.client.get('/recipe/7/')
        self.assertEqual(404,response.status_code)
        
    def test_display_recipe_happy_path(self):
        self.assertEqual(None,self.client.session.get('recipe_history'))
        response = self.client.get('/recipe/1/')
        self.assertContains(response,'foo') 
        self.assertEqual([['Recipe 1',1]],self.client.session['recipe_history'])
        
    def test_recipe_history_population(self):
        self.client.get('/recipe/1/')
        self.client.get('/recipe/2/')
        self.client.get('/recipe/3/')
        response = self.client.get('/recipe/4/')
        
        self.assertEqual(4,len(self.client.session['recipe_history']))
        self.assertEqual(['Recipe 2',2],self.client.session['recipe_history'][1])

        response = self.client.get('/recipe/2/')
        self.assertEqual(['Recipe 2',2],self.client.session['recipe_history'][1])
        
        response = self.client.get('/recipe/5/')
        self.assertEqual(4,len(self.client.session['recipe_history']))
        self.assertEqual(['Recipe 2',2],self.client.session['recipe_history'][0])
        self.assertEqual(['Recipe 5',5],self.client.session['recipe_history'][3])
        
    def test_existing_recipe_with_wrong_user(self):
        response = self.client.get('/recipe/6/')
        self.assertEqual(403,response.status_code)        
        
class TestEditView(TestCase):
    def setUp(self):
        Recipe.objects.create(title="Delicious Recipe",user_id=1)
        self.user = User.objects.create(username="testuser")
        self.user.set_password("secret")
        self.user.save()
        self.client = Client()
        self.client.force_login(self.user)
        
    def test_edit_existing_title_success(self):
        value = 'New Title'
        response = self.client.post('/recipe/edit/',{'name':'title','value':value,'pk':'1'})
        self.assertEqual(200,response.status_code)
        recipe = Recipe.objects.get(pk=1)
        self.assertEqual(value,recipe.title)
        
    def test_edit_existing_title_no_recipe(self):
        response = self.client.post('/recipe/edit/',{'name':'title','value':'New Title','pk':'2'})
        self.assertEqual(200,response.status_code)
        json_response = json.loads(response.content)
        self.assertEqual(json_response['status'],'error')
        self.assertEqual(json_response['msg'],'The recipe could not be found.')
        
    def test_create_new_recipe(self):
        value = 'New Recipe'
        response = self.client.post('/recipe/edit/',{'name':'title','value':value})
        self.assertEqual(200,response.status_code)
        recipe = Recipe.objects.get(pk=2)
        self.assertEqual(value,recipe.title)
        self.assertEqual('Add ingredients here.',recipe.ingredients)
        
    def test_edit_wrong_user(self):
        Recipe.objects.create(title="Wrong Recipe",user_id=2)
        response = self.client.post('/recipe/edit/',{'name':'title','value':'New Title','pk':'2'})
        self.assertEqual(200,response.status_code)
        json_response = json.loads(response.content)
        self.assertEqual(json_response['status'],'error')
        self.assertEqual(json_response['msg'],'The recipe could not be found.')
        
    def test_edit_missing_fields(self):
        response = self.client.post('/recipe/edit/',{'name':'title','pk':'1'})
        self.assertEqual(200,response.status_code)
        json_response = json.loads(response.content)
        self.assertEqual(json_response['status'],'error')
        self.assertEqual(json_response['msg'],'There was a problem saving the recipe.')
        