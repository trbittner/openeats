from django.conf.urls import url
from openeats.ingredient.views import autocomplete_ing

urlpatterns = [url(r'^auto/$', autocomplete_ing)]