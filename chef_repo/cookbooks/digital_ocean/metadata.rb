name 'digital_ocean'
maintainer 'Todd Bittner'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Configures components for Digital Ocean Ubuntu instances and installs wsgi for python applications.'
long_description 'Installs and configures a hardened server for anything running on a modern version of Ubuntu and installs wsgi components.'
version '3.5'

# If you upload to Supermarket you should set this so your cookbook
# gets a `View Source` link
# source_url 'https://github.com/<insert_org_here>/digital_ocean' if respond_to?(:source_url)
