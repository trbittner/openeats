remote_file '/usr/local/sbin/certbot-auto' do
source 'https://dl.eff.org/certbot-auto'
    mode '0770'
    action :create_if_missing
end

execute 'Enable ssl' do
    command 'sudo a2enmod ssl'
end

execute 'Enable key caching' do
    command 'sudo a2enmod socache_shmcb'
end

execute 'Enable rewrite rules' do
    command 'sudo a2enmod rewrite'
end

droplet_data = data_bag_item('sysadmins','digital_ocean_droplet')

template '/etc/apache2/sites-available/home-bistro.conf' do
    source 'home-bistro.conf.erb'
    action :create
    variables({
        :from => droplet_data['from'],
        :domain => droplet_data['domain'],
        :disable_ssl => node['disable_ssl']
        })
end

template '/etc/apache2/sites-available/home-bistro-le-ssl.conf' do
    source 'home-bistro-le-ssl.conf.erb'
    action :create
    variables({
        :from => droplet_data['from'],
        :domain => droplet_data['domain'],
        :disable_ssl => node['disable_ssl']
        })
end

unless File.exists?("/etc/letsencrypt/live/#{droplet_data['domain']}/privkey.pem")
    execute 'Run letsencrypt to get a certificate' do
        command(
            "sudo /usr/local/sbin/certbot-auto certonly --apache -n" + 
            " -d #{droplet_data['domain']} -m #{droplet_data['to']} --agree-tos --redirect")
    end
end

cookbook_file '/etc/letsencrypt/options-ssl-apache.conf' do
    source 'options-ssl-apache.conf'
end 

execute 'Enable SSL Host' do
    command 'sudo a2ensite home-bistro-le-ssl.conf'
end

cron 'renew certificate' do
    minute '30'
    hour '2'
    user node['user_name']
    mailto droplet_data['to']
    command "sudo sh -c '/usr/local/sbin/certbot-auto renew >> /var/log/le-renew.log'"
end

# Use this with knife ssh when disabling ssl as the command to execute: 'echo "{\"disable_ssl\": true}" | sudo chef-client -j /dev/stdin'
if node['disable_ssl']
    execute 'Disable ssl' do
        command 'sudo a2dismod ssl'
    end

    execute 'Disable SSL Host' do
        command 'sudo a2dissite home-bistro-le-ssl.conf'
    end
    
    execute 'Enable http Host' do
        command 'sudo a2ensite home-bistro.conf'
    end    
end

service 'apache2' do
    action :restart
end
