from django.conf.urls import url
from openeats.tags import views as tags_views

urlpatterns = [url(r'^recipe/(?P<tag>[-\w]+)/$', tags_views.recipeTags, name="recipe_tags")]