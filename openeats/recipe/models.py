from django.db import models
from django.utils.translation import ugettext_lazy as _

class Recipe(models.Model):
    title = models.CharField(_("Recipe Title"), max_length=250, default="Add a title here.")
    ingredients = models.TextField(_('ingredients'), default='Add ingredients here.')
    instructions = models.TextField(_('instructions'), default='Add instructions here.')
    description = models.TextField(_('description'), default='Add a description here.')
    user_id = models.IntegerField(_('user_id'),default=0)
    image_location = models.ImageField('image', default='')
    thumbnail_location = models.ImageField('thumbnail', default='')
    
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['title']

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/recipe/%s/" % str(self.id)
    